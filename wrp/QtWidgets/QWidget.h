// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

class QWidget : public QObject
{
public:
    QWidget(QWidget *parent = Q_NULLPTR);

public:
    void setLayout(QLayout *layout);

public:
    void show(void);
};

//
// QWidget.h ends here
