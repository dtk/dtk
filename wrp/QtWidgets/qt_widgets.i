// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

%module qtwidgets

%include "qt_core.i"

%{

#include <QtWidgets>

%}

%include "QLayout.h"
%include "QBoxLayout.h"
%include "QVBoxLayout.h"
%include "QWidget.h"

//
// qt_widgets.i ends here
