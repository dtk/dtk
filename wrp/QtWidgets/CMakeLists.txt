## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/QBoxLayout.h  DESTINATION wrp/QtWidgets/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/QVBoxLayout.h DESTINATION wrp/QtWidgets/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/QLayout.h     DESTINATION wrp/QtWidgets/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/QWidget.h     DESTINATION wrp/QtWidgets/)
install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/qt_widgets.i  DESTINATION wrp/QtWidgets/)

######################################################################
### CMakeLists.txt ends here
