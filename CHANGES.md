# ChangeLog
## version 1.7.1 - 2019-01-24
- add isatty method in python redirector
## version 1.7.0 - 2019-01-24
- add clear method in factory
- add option to replace plugin in factory
- add threading options to pythonInterpreter
- add option to redirect output in pythonInterpreter and use a specific settings file
## version 1.6.7 - 2018-11-23
- Swig: Add typecheck for QStringList
## version 1.6.6 - 2018-11-22
- Remove Python->C++ director swig conversions for references on Qt implicitly shared classes.
## version 1.6.5 - 2018-11-20
- Swig: set bool conversion prior to int in order to recover boolean and not integers
## version 1.6.4 - 2018-11-09
- fix for QVariant wrapper mapping
## version 1.6.3 - 2018-11-08
- fix for conda and DTK_INSTALL_PREFIX
## version 1.6.2 - 2018-11-08
- dtkScriptInterpreterPython enables release method (for real).
## version 1.6.1 - 2018-11-08
- dtkScriptInterpreterPython enables release method.
## version 1.6.0 - 2018-11-07
- dtkScriptInterpreterPython is now a singleton.
## version 1.5.2 - 2018-11-05
- Return default value if view is not instanciated.
## version 1.5.1 - 2018-07-25
- export `DTK_PYTHON_INSTALL_PATH` variable
## version 1.5.0 - 2018-07-25
- API change: add acces to factories and manager in the layer manager; add *loadFromName* in *dtkCorePluginManagerBase*
- change python modules directory (from `modules` to `lib/pythonX.Y/site-packages`)
## version 1.4.4 - 2018-07-19
- fix typo when installating dtkWidgets sip wrapping on macOS
## version 1.4.3 - 2018-07-19
- fix sip wrapping installation of dtkWidgets and dtkCore
- fix pythonappend in swig for dtkObjectManager
## version 1.4.2 - 2018-03-14
- fix dtkConfig when installing
## version 1.4.1 - 2018-03-09
- fix cmake for dtkWrap when dtk is used as a submodule
## version 1.4.0 - 2018-03-08
- major update to python wrapping: enhance swig (now we can implement pure python plugins) + add sip wrapping for dtkWidgets. update to dtk_wrap macro API
- no longer use find_packages for Qt in dtkConfig
- add dtkObjectManager
- add dtkViewManager and associated classes
- add dtkCorePluginWidgetManager as a singleton that stores a map between concept objects encapsulated within a QVariant and widgets that have to be provided by the plugins. This enable plugins that have specific setters and getters to be handled in the composer through widget editors. This mechanism replace dtkWidgetFactory
- minor bugfixes
- **this will be the last version of DTK that will include the support layers **
