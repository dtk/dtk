// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport.h>

#include <QtCore>

class dtkViewWidget;

class DTKWIDGETS_EXPORT dtkViewController : public QObject
{
    Q_OBJECT

public:
    static dtkViewController *instance(void);

public:
    void insert(dtkViewWidget *);

public:
    dtkViewWidget *view(const QString& name);

public:
    QStringList viewNames(void);

signals:
    void inserted(dtkViewWidget *, const QString&);

protected:
     dtkViewController(void);
    ~dtkViewController(void);

private:
    static dtkViewController *s_instance;

private:
    class dtkViewControllerPrivate *d;
};

//
// dtkViewController.h ends here
