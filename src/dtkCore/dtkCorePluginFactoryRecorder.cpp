// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkCorePluginFactoryRecorder.h"

#include "dtkCoreLayerManager.h"

// /////////////////////////////////////////////////////////////////
// dtkCorePluginFactoryRecorder implementation
// /////////////////////////////////////////////////////////////////

dtkCorePluginFactoryRecorder::dtkCorePluginFactoryRecorder(dtkCoreLayerManager *layer_manager, dtkCorePluginFactoryBase *plugin_factory, const QString& plugin_name)
{
    layer_manager->record(plugin_name, plugin_factory);
}

//
// dtkCorePluginFactoryRecorder.cpp ends here
