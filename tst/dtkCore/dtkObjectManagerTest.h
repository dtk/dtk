// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkTest>


class dtkObjectManagerTestCasePrivate;

class dtkObjectManagerTestCase : public QObject
{
    Q_OBJECT

public:
             dtkObjectManagerTestCase(void);
    virtual ~dtkObjectManagerTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCount(void);
    void testValue(void);
    void testKeys(void);
    void testDel(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    dtkObjectManagerTestCasePrivate *d;
};

//
// dtkObjectManagerTest.h ends here
