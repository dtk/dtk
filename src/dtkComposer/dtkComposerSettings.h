// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkComposerExport.h>

#include <QtCore>

class DTKCOMPOSER_EXPORT dtkComposerSettings : public QSettings
{
public:
    dtkComposerSettings(void);
    ~dtkComposerSettings(void);
};

//
// dtkComposerSettings.h ends here
