class QVariant {
public:
    QVariant();
    QVariant(int);
    QVariant(const char *);
    QVariant(bool);
    int toInt() const;
    QString toString() const;
    QStringList toStringList() const;
    bool toBool() const;
    const char *typeName() const;
};

%extend QVariant {
    void setValue(int value) {
        $self->setValue(QVariant::fromValue(value));
    }
}
